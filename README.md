# OrcaSequestration

A bunch of scripts for managing a fleet of Plom Server containers.


## How to use this: preparation

Make any changes needed to the `compose.yaml` file in `factory/`.  This file
lightly modified from the Plom source code.

  * Pull the desired version of the `plomgrading/server` container image.
  * Customize whatever you need to pull in SSL certs for NginX.
  * Set the `BASE_CONTAINER_NAME` in `orca-config.env`.
  * Choose initial port number in `next-port-number.txt`.


## Launching a new container

Run `orca-new`.  It will choose the port number from
`next-port-number.txt` and copy the factory to make a new instance.

  * 41984 in `<newdir>/compose.yaml` will be changed to the chosen port.
  * A new secret key will be randomly generated.

Notes:

  * There is a PostgreSQL database and a storage volume.
  * Note the printed manager and perhaps the admin password.

You do *not* need to use a multiplexer such as GNU Screen: you can
Ctrl-C and you're only interrupting the log watcher, which can be
connected again with `orca-log`.


## Stopping a container and/or restarting

`orca-down <port>`: it takes about 10 seconds to shutdown the Huey queue.

You can hot-start it again with `./orca-up <port>`, for example after
bumping **patch-level** of your Plom container image, which is
supposed to be a safe thing to do.


## Completely removing a container and its volumes

`orca-wipe <port_number>`: this is dangerous with loss of data: it will warn you.


## Archiving/backups

Need to backup the clone of factory.  Need to backup the two volumes.


## Restoring

Need to restore the two volumes.  You can hack the port number in the compose.yaml
file if you want to bring it up on a different port.  Any existing password reset links
are likely to fail.   Potentially other port-specific stuff, although there ideally
shouldn't be any/much.
