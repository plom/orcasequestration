#!/bin/bash
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (C) 2024 Colin B. Macdonald

# dependencies: docker-compose, docker, rsync, bash

set -e

source orca-config.env

if [ "x$1" = "x" ]; then
  echo "Usage: $0 <port_num>"
  echo "Launch a production Plom server in demo mode"
  exit 1
fi

export PORT=$1
D=${BASE_CONTAINER_NAME}${PORT}
echo "using port $PORT and dir ${D}"
sleep 1
mkdir -p ${D}
# dunno how to exclude with copy so use rsync
rsync -auv factory.demo/ ${D}/ --exclude .git
# hack port numbers in the compose file
sed -i "s/41984/$PORT/" ${D}/compose.yaml
# Custom SSL cert import
cp -i /etc/ssl/certs/vplom.crt $D/nginx/
sudo cp -i /etc/ssl/private/vplom.key $D/nginx/
pushd ${D}
cat compose.yaml | grep $PORT

echo "*********************************************"
sudo docker-compose down
echo "*********************************************"
sudo docker-compose build
echo "*********************************************"
sudo docker-compose up

popd
